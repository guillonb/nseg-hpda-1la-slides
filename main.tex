\makeatletter
\def\input@path{ {./} {sty/} {fig/} {tikz/} {cls/} {tex/} }
\makeatother
\documentclass[italian,UKenglish]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{babel}

\usepackage{amsmath,amsthm,amssymb,mathtools}
\usepackage[inline]{enumitem}
\setitemize{label=\usebeamerfont*{itemize item}\usebeamercolor[fg]{itemize item}\usebeamertemplate{itemize item}}

\usepackage[mode=image|tex]{standalone}

% local packages
\usepackage{mybeamer}
\usepackage{tikzemph,tikzloops,tikztools,tikzlayer}
\usetikzlibrary{decorations.pathreplacing,arrows,fit}
\usepackage{leftright}
\usepackage{macros}
\usepackage{globalstyles}

\setcounter{tocdepth}{4}

% title
\title{%
	Non-Self-Embedding Grammars,\newline
	Constant-Height Pushdown Automata,\newline
	and Limited Automata%
}
\author{%
	\underline{Bruno Guillon}
	\and
	Giovanni Pighizzini
	\and
	Luca Prigioniero%
}
\date{%
	{%
		\small%
		\textsc{ciaa}%
	}%
	\newline%
	{%
		\scriptsize
		August 1, 2018%
	}%
}
\institute{%
	{\selectlanguage{italian}{Dipartimento di Informatica, Università degli Studi di Milano}}%
}


\begin{document}
\addtocounter{framenumber}{-1}
\maketitle
\begin{frame}{Context-free ability: describe recursive structure}
	\vspace{-2ex}
	\begin{overlayarea}{\textwidth}{.55\textheight}%FIGURE HIERARCHY
		\widebox{%
			\centering
			\foreach \time [count=\page from 1] in {1-3,4-5,6,7-46,47-}{%
				\only<\time>{\includegraphics[page=\page,scale=1]{chomsky}}%
			}%
		}%
	\end{overlayarea}
	\vspace{-2ex}
	\begin{overlayarea}{\textwidth}{.125\textheight}%EXAMPLES
		\only<2-6>{%
			\widebox{%
				\hfill
				\begin{minipage}{.5\textwidth}
					\centering
					$\grmC{\mathbf G}:\,\grmC S\to\Odick\grmC S\Cdick|\grmC S\grmC S|\emptyword$
				\end{minipage}
				\hfill
				\begin{minipage}{.5\textwidth}
					\vspace{-1ex}
					\begin{tikzpicture}
						\path
							(0,0)					coordinate (q1)
							++(15:12.5mm)		coordinate (q2)
							(q2)	++(0:2cm)	coordinate (q3)
							++(345:12.5mm)		coordinate (q4)
							%
							(q1)	edge[transition]	node[above,sloped,font=\scriptsize]	{$\Odick\mid\mchC\pdpush$}	(q2)
							(q3)	edge[transition]	node[above,sloped,font=\scriptsize]	{$\Cdick\mid\mchC\pdpop$}		(q4)
							%
							(q2)	edge[dotted,in=165,out=15,looseness=2]	(q3)
							;
					\end{tikzpicture}
				\end{minipage}
				\hfill\vbox{}
			}
		}
		\only<8->{%
			\hfill
			\foreach \time [count=\page from 1] in {8,...,45,46-}{%
				\only<\time>{\includegraphics[page=\page,width=.75\textwidth]{2la4dick2}}%
			}%
			\hfill\vbox{}
			\vspace{-4ex}

			\uncover<46->{\alert<46>{\hfill Cells can be rewritten only in the first~2 visits!\quad}}%
		}
	\end{overlayarea}
	\begin{overlayarea}{\textwidth}{.5\textheight}%DEFINITIONS
		\only<2-6>{%
			\widebox{%
				\hfill
				\begin{minipage}[t]{.5\textwidth}
					\begin{definition}<3->[{\nse\ {\footnotesize[Chomsky~1959]}}]
						\small
						$\grmC{\mathbf G}$ is \alert<3>{\emph{self-embedding}}
						if~for~some~$X$,
						$X\xRightarrow*\alpha X\beta$
						with both~$\alpha,\beta$ nonempty.%
						\medbreak

						Otherwise,
						$\grmC{\mathbf G}$ is \alert<4>{non-self-embedding}.%
					\end{definition}
				\end{minipage}
				\hfill
				\begin{minipage}[t]{.5\textwidth}
					\begin{definition}<5->[\hpda]
						\small
						An \alert<6>{\emph{$h$-height \pda}}
						is a \pda\linebreak
						with stack size~$\leq h\in\mathbb N$.
					\end{definition}
				\end{minipage}
				\hfill\vbox{}
			}
		}
		\only<7->{%
			\vspace{4ex}

			\widebox[0]{%
				\begin{inlinedefinition}[Hibbard~1967]
					\hfill
					For~$d\in\mathbb N$, a \la[d] is a \ottm
					\smallbreak

					\hfill
					allowed to rewrite a cell content only during its first~$d$ visits.%
				\end{inlinedefinition}
			}
		}
	\end{overlayarea}
\end{frame}
\begin{frame}{Concise representations of regular languages}
	\widebox{%
		\centering
		\foreach \time [count=\page from 6] in {1,...,7}{%
			\only<\time>{\includegraphics[page=\page,scale=1]{chomsky}}%
		}%
	}%
	\begin{overlayarea}{\textwidth}{.3\textheight}
		\widebox{%
			\begin{inlinedefinition}<2->
				Sizes of models:
				\medbreak

				\hfill
				\begin{minipage}[t]{.33\textwidth}
					\centering
					\grmC{grammars}

					$\sum\limits_{X\to\alpha\in P}(2+\length\alpha)$
				\end{minipage}
				\hfill
				\begin{minipage}[t]{.33\textwidth}
					\centering
					\mchC\hpda

					poly in $\card\stateset$, $\card\pdalphab$,~$h$
				\end{minipage}
				\hfill
				\begin{minipage}[t]{.33\textwidth}
					\centering
					\mchC\la

					poly in $\card\stateset$, $\card\walphab$
				\end{minipage}
				\hfill\vbox{}
				\bigskip

				\uncover<2-3>{%
					\hfill\hfill
					\mchC\fa: poly in $\card\stateset$%
					\hfill\vbox{}%
				}%
			\end{inlinedefinition}
		}%
	\end{overlayarea}
\end{frame}
\begin{frame}{From \nseg to \hpda and back}
	\begin{overlayarea}{\textwidth}{.3\textheight}
		\widebox{%
			\hfill
			\begin{minipage}[b]{.34\textwidth}
				\begin{block}{Production graph}
					There is an edge $X\to Y$ if there is a production $\grmC X\to\alpha\grmC Y\beta$
				\end{block}
			\end{minipage}
			\hfill
			\begin{minipage}[b]{.70\textwidth}
				\foreach \time [count=\page] in {1,2-}{%
					\only<\time>{\includegraphics[width=\textwidth,page=\page]{production-graph}}%
				}%
			\end{minipage}
			\hfill\vbox{}
		}
	\end{overlayarea}
	\vfill

	\begin{overlayarea}{\textwidth}{.4\textheight}
		\rightwidebox[.75]{%
			\begin{itemize}
				\item<3->
					each SCC defines a left- or right-linear grammar

					\hfill
					\textcolor{darkgray}{[Anselmo, Giammarresi, Varricchio 2002]}
				\item<4->
					with a polynomial size increase,%

					\hfill
					we can assume that each such SCC-grammar is right-linear
				\item<5->
					the resulting grammar can in turn be transformed

					\hfill
					into an \hpda of polynomial size
					\hfill
					{\small(\emph{adapting} \textcolor{darkgray}{[AGV02]})}
			\end{itemize}
		}
	\end{overlayarea}

	\begin{overlayarea}{\textwidth}{.3\textheight}
		\rightwidebox{%
			\begin{itemize}
				\item<6->
					Conversely,
					we can transform each \hpda into a poly-size \nseg

					\uncover<7->{%
						of particular form:
					}
					\smallbreak

					\hfill
					\uncover<8->{%
						\alert<9->{%
							CNF in which each production $X\to YZ$
							is such that~$Y>X$%
						}%
					}
					\hfill\hfill\vbox{}
			\end{itemize}
		}
	\end{overlayarea}
\end{frame}
\begin{frame}{From \nseg to \la}
	\tikz[overlay,remember picture]
	\path (current page.north east)	node[below left,align=left,draw=gbbrown,shape=rectangle,rounded corners]
	{from \nseg in CNF with\\$X\to YZ\implies Y>X$};

	\widebox[1]{%
		\hspace{-2pt}%
		\foreach \time [count=\page] in {1,...,23}{%
			\only<\time>{\includegraphics[page=\page,width=1.055\paperwidth]{dtree}}%
		}%
	}%
\end{frame}
\begin{frame}{Simulation of \la: an exponential gap}
	\begin{overlayarea}{\textwidth}{.5\textheight}
		\widebox{%
			\centering
			\foreach \time [count=\page from 11] in {1,2,3,4,5,6-}{%
				\only<\time>{\includegraphics[page=\page,scale=1]{chomsky}}%
			}%
		}%
	\end{overlayarea}
	\begin{overlayarea}{\textwidth}{.4\textheight}%
		\begin{inlinetheorem}<4->
			\hfill
			$L_n=\set*{u^n}[n\in\mathbb N,\ u\in\set{a,b}^n]$
			\hfill\hfill\vbox{}
			\medbreak
			\begin{itemize}
				\item accepted by a \alert<4>{\dfa[2]{}} with~\bigo n states
				\item for which a \pda or a \cfg requires a size exponential in $n$
			\end{itemize}
		\end{inlinetheorem}
	\end{overlayarea}
	\begin{tikzpicture}[overlay,remember picture,visible on={<7->}]%
		\path (current page.south east)	++(90:2ex) ++(180:2em)	node[above left,shape=rectangle,draw=gbbrown]	{Thank you for your attention.};%
	\end{tikzpicture}%
\end{frame}
\end{document}

